#!/bin/bash

echo running post install scripts for web..;

if [ "$(ls -A /var/www/html)" ]; then
	echo "Drupal already installed"

	echo "Waiting for $SITE_DB_URL:$SITE_DB_PORT..."
	eval "./scripts/wait-for-it.sh $SITE_DB_URL:$SITE_DB_PORT"
else
	echo "Downloading Drupal $DRUPAL_VERSION"
	eval "drush dl drupal-$DRUPAL_VERSION --destination=/var/www --drupal-project-rename=html -y"
	chown -R www-data:www-data /var/www/html/sites

	echo "Waiting for $SITE_DB_URL:$SITE_DB_PORT..."
	eval "./scripts/wait-for-it.sh $SITE_DB_URL:$SITE_DB_PORT"

	echo "Instaling Drupal $DRUPAL_VERSION"
	eval "drush site-install standard --db-url='$SITE_DB://$DB_USER:$DB_PASSWORD@$SITE_DB_URL/$SITE_DB_TABLE' --site-name=$SITE_NAME --account-pass=$SITE_ADMIN_PASS -y"
	chown -R www-data:www-data /var/www/html/sites
	if [[ $DRUPAL_VERSION == 7* ]]; then
		eval "drush cc all"
	else
		eval "drush cache-rebuild"
	fi
fi

if [ -f /scripts/wait-for-it.sh ]; then
	echo Mapping UIDs...
	chown -R www-data:www-data /var/www/html
	OLDUID=`id -u www-data`
	NEWUID=`stat -c '%u' /scripts/wait-for-it.sh`
	usermod -u $NEWUID www-data
	find /var/www/html -user $OLDUID -exec chown -h $NEWUID {} \;
fi

if $USE_DB_DUMP; then
	if [ -f /db-inport/$SITE_DB_TABLE.sql ]; then
		eval "drush sql-drop -y"
		eval "drush sql-cli < /db-inport/$SITE_DB_TABLE.sql"

		if [[ $DRUPAL_VERSION == 7* ]]; then
			eval "drush cc all"
		else
			eval "drush cache-rebuild"
		fi
	else
		echo "File db-inport/$SITE_DB_TABLE.sql does not exist! Import failed!"
	fi
fi

apache2-foreground
