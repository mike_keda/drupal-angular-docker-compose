import {bootstrap} from 'angular2/platform/browser';
import {DrupalAngular2App} from './app/drupal-angular2';
import {ROUTER_PROVIDERS} from 'angular2/router';

bootstrap(DrupalAngular2App, [
  ROUTER_PROVIDERS
]);
