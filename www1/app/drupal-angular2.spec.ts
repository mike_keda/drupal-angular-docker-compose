import {describe, it, expect, beforeEachProviders, inject} from 'angular2/testing';
import {DrupalAngular2App} from '../app/drupal-angular2';

beforeEachProviders(() => [DrupalAngular2App]);

describe('App: DrupalAngular2', () => {
  it('should have the `defaultMeaning` as 42', inject([DrupalAngular2App], (app: DrupalAngular2App) => {
    expect(app.defaultMeaning).toBe(42);
  }));

  describe('#meaningOfLife', () => {
    it('should get the meaning of life', inject([DrupalAngular2App], (app: DrupalAngular2App) => {
      expect(app.meaningOfLife()).toBe('The meaning of life is 42');
      expect(app.meaningOfLife(22)).toBe('The meaning of life is 22');
    }));
  });
});

